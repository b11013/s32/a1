const http=require('http');



let items = [

	{
		name: "Iphone X",
		price: 30000,
		isActive: true
	},
	{
		name: "Samsung Galaxy S21",
		price: 51000,
		isActive: true
	},
	{
		name: "Razer Blackshark VX2",
		price: 2800,
		isActive: false
	}
];

http.createServer((req, res) =>{
		if (req.url === '/items' && req.method	=== 'GET')
		{
			res.writeHead	(200, {'Content-Type':'application/json'})
			res.end	(JSON.stringify(items));

		} 
		else if (req.url === '/items' && req.method === 'POST') {

			let requestBody = '';

			req.on('data', (data) =>{
				requestBody += data
			})

			req.on('end', () =>{
					requestBody = JSON.parse(requestBody)

				let newItems = {
					name: requestBody.name,
					price: requestBody.price,
					isActive: requestBody.isActive
				}
				items.push(newItems);

				res.writeHead(200, {'Content-Type':'application/json'})
				res.end(JSON.stringify(items))

			})
		}

else if (req.url === '/items' && req.method	=== 'DELETE')
		{
			res.writeHead	(200, {'Content-Type':'application/json'})
			items.pop();
			res.end	(JSON.stringify(items));

		} 




}).listen(8000)

console.log	('Server is running at port 8000')

